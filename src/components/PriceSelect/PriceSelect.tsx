import React from 'react';

import Select from 'react-select';

import styles from './styles.module.scss';

function YearSelect() {
  return (
    <Select className={styles.defaultSelect} placeholder="Ano Desejado" />
  );
}

export default YearSelect;