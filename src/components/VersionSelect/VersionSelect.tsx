import React, { useState, useEffect } from 'react';

import Select from 'react-select';
import axios from 'axios';

import styles from './styles.module.scss';

interface VersionModel {
  ModelID: number;
  ID: number;
  Name: string;
}

function VersionSelect() {
  const [versions, setVersions] = useState<VersionModel[]>([]);

  useEffect(() => {
    axios.get('https://desafioonline.webmotors.com.br/api/OnlineChallenge/Version?ModelID=1')
      .then(function (response) {
        setVersions(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  let options = versions.map(function (version) {
    return { value: version.ID, label: version.Name };
  })

  return (
    <Select className={styles.defaultSelect} placeholder="Versão:" options={options} />
  );
}

export default VersionSelect;