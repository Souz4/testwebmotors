import React from 'react';

import Select from 'react-select';

import styles from './styles.module.scss';

const options = [
  { value: '5 km', label: '5 km' },
  { value: '10km', label: '10km' },
  { value: '25km', label: '25km' },
  { value: '50km', label: '50km' },
  { value: '75km', label: '75km' },
  { value: '100km', label: '100km' }
]

function RaySelect() {
  return (
    <Select className={styles.defaultSelect} placeholder="Raio:" options={options} />
  );
}

export default RaySelect;