import React, { useState, useEffect } from 'react';

import Select from 'react-select';
import axios from 'axios';

import styles from './styles.module.scss';

interface MakeModel {
  ID: number;
  Name: string;
}

function MakeSelect() {
  const [makes, setMakes] = useState<MakeModel[]>([]);

  useEffect(() => {
    axios.get('https://desafioonline.webmotors.com.br/api/OnlineChallenge/Make')
      .then(function (response) {
        setMakes(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  let options = makes.map(function (make) {
    return { value: make.ID, label: make.Name };
  })

  return (
    <Select className={styles.defaultSelect} placeholder="Marca:" options={options} />
  );
}

export default MakeSelect;