import React from "react";

import styles from "./styles.module.scss";

interface Props {
  name: string;
}

const Button: React.FunctionComponent<Props> = ({ name }) => {
  return (
    <button className={styles.primaryBtn}>{name}</button>
  );
}

export default Button;