import React, { useState, useEffect } from 'react';

import Select from 'react-select';
import axios from 'axios';

import styles from './styles.module.scss';

interface StateModel {
  id: number;
  sigla: string;
  nome: string;
}

function StateSelect() {
  const [states, setStates] = useState<StateModel[]>([]);

  useEffect(() => {
    axios.get('https://servicodados.ibge.gov.br/api/v1/localidades/estados')
      .then(function (response) {
        setStates(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  let options = states.map(function (state) {
    return { value: state.id, label: state.nome + ` - ` + state.sigla };
  })

  return (
    <Select className={styles.defaultSelect} placeholder="Onde:" options={options} />
  );
}

export default StateSelect;