import React from 'react';

import Select from 'react-select';

import styles from './styles.module.scss';

function PriceSelect() {
  return (
    <Select className={styles.defaultSelect} placeholder="Faixa de Preço" />
  );
}

export default PriceSelect;