import React, { useState, useEffect } from 'react';

import Select from 'react-select';
import axios from 'axios';

import styles from './styles.module.scss';

interface CarModel {
  MakeID: number;
  ID: number;
  Name: string;
}

function ModelsSelect() {
  const [models, setModels] = useState<CarModel[]>([]);

  useEffect(() => {
    axios.get('https://desafioonline.webmotors.com.br/api/OnlineChallenge/Model?MakeID=1')
      .then(function (response) {
        setModels(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  let options = models.map(function (model) {
    return { value: model.ID, label: model.Name };
  })

  return (
    <Select className={styles.defaultSelect} placeholder="Modelo:" options={options} />
  );
}

export default ModelsSelect;