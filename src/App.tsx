import React, { useState, useEffect } from 'react';

import axios from 'axios';

import styles from './styles/global.module.scss';

import Button from './components/Button/Button';
import StateSelect from './components/StateSelect/StateSelect';
import MakeSelect from './components/MakeSelect/MakeSelect';
import ModelSelect from './components/ModelSelect/ModelSelect';
import RaySelect from './components/RaySelect/RaySelect';
import YearSelect from './components/YearSelect/YearSelect';
import PriceSelect from './components/YearSelect/YearSelect';
import VersionSelect from './components/VersionSelect/VersionSelect';

interface StateModel {
  id: number;
  nome: string;
  sigla: string;
}

function App() {
  const [states, setStates] = useState<StateModel[]>([]);

  useEffect(() => {
    axios.get('https://servicodados.ibge.gov.br/api/v1/localidades/estados')
      .then(function (response) {
        // console.log(response.data);
        setStates(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });

  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <img src="logo.png" className={styles.logoHeader} alt="logo webmotors" />
      </div>
      <div className={styles.boxForm}>
        <div className={styles.headerForm}>
          <ul className={styles.nav}>
            <li className={styles.navOptions}>
              <div className={styles.linkOption}>
                <img src="icon_car.svg" className={styles.iconOption} alt="ìcone de Carro" />
                <div className={styles.labelOption}>
                  <span className={styles.neutralSpan}>Comprar</span>
                  <span className={styles.dinamicSpan}>Carros</span>
                </div>
              </div>
            </li>
            <li className={styles.navOptions}>
              <div className={styles.linkOption}>
                <img src="icon_motorcycle.svg" className={styles.iconOption} alt="ìcone de Moto" />
                <div className={styles.labelOption}>
                  <span className={styles.neutralSpan}>Comprar</span>
                  <span className={styles.dinamicSpan}>Motos</span>
                </div>
              </div>
            </li>
          </ul>

          <div className={styles.actionButton}>
            <button className={styles.outlineAlertBtn}>Vender meu Carro</button>
          </div>
        </div>

        <div className={styles.bodyForm}>
          <div className={styles.contentForm}>
            <div className={styles.row}>
              <div className={styles.checkFilter}>
                <input type="checkbox" className={styles.checkboxes} id={styles.newOption} /><span className={styles.checkSpan}>Novos</span>
              </div>
              <div className={styles.checkFilter}>
                <input type="checkbox" className={styles.checkboxes} id={styles.usedOption} /><span className={styles.checkSpan}>Usados</span>
              </div>
            </div>
            <div className={styles.rowItems}>
              <StateSelect />
              <RaySelect />
              <MakeSelect />
              <ModelSelect />
            </div>
            <div className={styles.rowItems}>
              <YearSelect />
              <PriceSelect />
              <VersionSelect />
            </div>
            <div className={styles.actionFilters}>
              <span className={styles.advancedButton}>Busca Avançada</span>
              <span className={styles.clearButton}>Limpar filtros</span>
              <Button name={"VER OFERTAS"} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
